# GitLab 13.12 Web Application Firewall

This is an extraction of Ingress model with Helm configuration to enable ModSecurity firewall as of GitLab 13.12.

This allows you to install Ingress with ModSecurity feature enabled if you want to continue to use it.

## How to use

You can install Ingress with modsecurity in your cluster with helm:

1. Copy `ingress-with-modsecurity-blocking.yml` (if you want to use modsecurity in blocking mode) or `ingress-with-modsecurity-logging.yml`
   (if you want to use modsecurity in logging mode) as values.yaml

2. With Helm 3 installed run these commands:

```bash
helm repo add ingress https://gitlab-org.gitlab.io/cluster-integration/helm-stable-archive
helm repo update
helm upgrade ingress ingress/nginx-ingress --install --atomic --cleanup-on-fail --reset-values --version 1.40.2 --set rbac.create\=true,rbac.enabled\=true --namespace gitlab-managed-apps -f values.yaml
```
